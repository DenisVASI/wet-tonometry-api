const passport = require('passport');
const LocalStrategy = require('passport-local');
const crypto = require('crypto');
const db = require('./database');
const query = require('../query/query');

const validatePassword = (password, salt, hash) => {
    return new Promise(resolve => {
        const local_hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
        return resolve(local_hash === hash);
    });
};

passport.use(new LocalStrategy({
    usernameField: 'user[email]',
    passwordField: 'user[password]',
}, async (email, password, done) => {

    const user = await db.query(query.getUser, [email]).then(res => res.rows[0]).catch(e => {
        console.log(e);
        return false;
    });

    if (user) {
        const isValid = await validatePassword(password, user.salt, user.hash);
        if (isValid) {
            return done(null, user);
        } else {
            return done(null, false, {errors: {'email or password': 'is invalid'}});
        }
    } else {
        return done(null, false, {errors: {'email or password': 'is invalid'}});
    }
}));