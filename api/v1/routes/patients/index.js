const auth = require('../auth');
const query = require('../../../../query/query');
const router = require('express').Router();
const db = require('../../../../config/database');

router.post('/list', auth.required, async (req, res) => {

    const {payload: {id}} = req;

    const list = await db.query(query.getPatientsList, [id])
        .then(res => res.rows)
        .catch(e => {
            console.log(e);
            return [];
        });

    res.json({patients: list});

});

module.exports = router;