const express = require('express');
const router = express.Router();
const auth = require('../auth');
const {createCanvas, Image} = require('canvas');

router.get('/test', function (req, res) {
    res.send('image processing ok!');
});

router.post('/draw', auth.optional, function (req, res) {
    if (!req.files && req.files.image) {
        res.send({
            status: 200,
            code: 1,
            errors: ['file not found']
        });
    } else {
        if (
            req.files.image.mimetype === 'image/jpeg' ||
            req.files.image.mimetype === 'image/jpg' ||
            req.files.image.mimetype === 'image/png'
        ) {

            //canvas init
            const canvas = createCanvas(2000, 2000);
            const ctx = canvas.getContext('2d');
            ctx.fillStyle = 'rgba(0,0,0,1)';
            ctx.fillRect(0, 0, 2000, 2000);
            ctx.fillStyle = 'rgba(255,255,255,1)';
            ctx.fillRect(150, 150, 1700, 1700);
            //canvas init

            //img load
            const img = new Image();
            img.onload = function () {
                ctx.drawImage(img, canvas.width / 2 - img.width / 2, canvas.height / 2 - img.height / 2);
                let base64 = canvas.toDataURL('image/jpeg');
                res.send({
                    base64: base64,
                    status: 200,
                    code: 0,
                    coordinates: {
                        x: canvas.width / 2 - img.width / 2,
                        y: canvas.height / 2 - img.height / 2
                    },
                    sizes: {
                        w: img.width,
                        h: img.height
                    }
                });
            };
            img.onerror = err => {
                throw err
            };
            img.src = req.files.image.data;
            //img load
        } else {
            res.send({
                status: 200,
                code: 2,
                errors: ['file with the file extension png or jpeg/jpg was not found']
            });
        }
    }
});

module.exports = router;