const auth = require('../auth');
const crypto = require('crypto');
const db = require('../../../../config/database');
const query = require('../../../../query/query');
const router = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');


const setPassword = (password) => {
    return new Promise(resolve => {
        const salt = crypto.randomBytes(16).toString('hex');
        const hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
        return resolve({salt, hash});
    });
};

const generateJWT = (id, email) => {
    return new Promise(resolve => {
        const today = new Date();
        const expirationDate = new Date(today);
        expirationDate.setDate(today.getDate() + 60);

        return resolve(jwt.sign({
            email: email,
            id: id,
            exp: parseInt(expirationDate.getTime() / 1000, 10),
        }, 'secret'));

    });
};

router.get('/test', function (req, res) {
    res.send('auth ok!');
});

router.post('/registration', auth.optional, async (req, res) => {

    const {body: {user}} = req;

    if (user && !user.email) {
        return res.status(422).json({
            errors: {
                email: 'is required',
            },
        });
    }

    if (user && !user.password) {
        return res.status(422).json({
            errors: {
                password: 'is required',
            },
        });
    }

    const {salt, hash} = await setPassword(user.password);

    const finalUser = {
        mail: user.email,
        hash: hash,
        salt: salt
    };

    const inserted = await db.query(query.insertUser, [finalUser.mail, finalUser.hash, finalUser.salt])
        .then(_ => {
            return {
                status: true
            }
        })
        .catch(e => {
            // console.log(e);
            return {
                status: false,
                error: e.detail
            };
        });

    if (inserted.status) {
        return res.json({
            user: finalUser.mail
        });
    } else {
        return res.status(422).json({
            errors: inserted.error,
        });
    }
});

router.post('/login', auth.optional, async (req, res, next) => {
    const {body: {user}} = req;

    if (user && !user.email) {
        return res.status(422).json({
            errors: {
                email: 'is required',
            },
        });
    }

    if (user && !user.password) {
        return res.status(422).json({
            errors: {
                password: 'is required',
            },
        });
    }

    return passport.authenticate('local', {session: false}, async (err, passportUser, info) => {
        if (err) {
            return next(err);
        }

        if (passportUser) {

            let user = passportUser;
            user.token = await generateJWT(user.id, user.mail);

            const {token} = user;

            return res.json({user: {token}});
        }

        return res.status(400).json(info);
    })(req, res, next);

});

router.get('/current', auth.required, (req, res) => {
    res.send('jwt test ok!');
});

module.exports = router;
