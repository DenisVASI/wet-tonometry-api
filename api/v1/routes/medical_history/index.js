const auth = require('../auth');
const query = require('../../../../query/query');
const router = require('express').Router();
const db = require('../../../../config/database');

router.post('/test', auth.optional, (req, res) => {
    res.send('medical history ok!');
});

router.post('/save', auth.required, async (req, res) => {

    const {body: {diagnosticsData}} = req;
    const {payload} = req;


    if (diagnosticsData && !diagnosticsData.image) {
        return res.status(422).json({
            errors: {
                image: 'is required',
            },
        });
    }


    if (diagnosticsData && !diagnosticsData.patientId) {
        return res.status(422).json({
            errors: {
                patient_id: 'is required',
            },
        });
    }


    if (diagnosticsData && !diagnosticsData.coordinates) {
        return res.status(422).json({
            errors: {
                coordinates: 'is required',
            },
        });
    }

    if (diagnosticsData && !diagnosticsData.masses) {
        return res.status(422).json({
            errors: {
                masses: 'is required',
            },
        });
    }

    if (diagnosticsData && !diagnosticsData.rations) {
        return res.status(422).json({
            errors: {
                rations: 'is required',
            },
        });
    }

    if (diagnosticsData && !diagnosticsData.drug_type) {
        return res.status(422).json({
            errors: {
                drug_type: 'is required',
            },
        });
    }

    if (diagnosticsData && !diagnosticsData.pressureSource) {
        return res.status(422).json({
            errors: {
                pressure_source: 'is required',
            },
        });
    }

    await db.query(query.insertResult, [
        diagnosticsData.image,
        payload.id,
        diagnosticsData.patientId,
        diagnosticsData.coordinates,
        diagnosticsData.masses,
        diagnosticsData.rations,
        diagnosticsData.drug_type,
        diagnosticsData.pressureSource
    ]).then(_ => {
        res.sendStatus(200);
    }).catch(e => {
        console.log(e);
        res.sendStatus(502);
    });

});

module.exports = router;