//libraries
const path = require('path');
const errorHandler = require('errorhandler');


//server
const express = require('express');
const fileupload = require("express-fileupload");
const cors = require('cors');
const bodyParser = require('body-parser');
const session = require('express-session');
const database = require('./config/database');
// require('./config/passport');

const config = require('./config/production');
const app = express();

//routes
const imageProcessing = require('./api/v1/routes/image_processing');
const auth = require('./api/v1/routes/auth/index');
const medicalHistory = require('./api/v1/routes/medical_history');
const patients = require('./api/v1/routes/patients');

//require app modules
app.use(require('morgan')('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileupload());
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(session({secret: config.secret, cookie: {maxAge: 60000}, resave: false, saveUninitialized: false}));
require('./config/passport');
const isProduction = process.env.NODE_ENV === 'production';

if (!isProduction) {
    app.use(errorHandler());
}

//require routes
app.use('/api/v1/images', imageProcessing);
app.use('/api/v1/history', medicalHistory);
app.use('/api/v1/patients', patients);
app.use('/api/v1/auth', auth);

(async () => {
    await database.connect();
    app.listen(config.port, function () {
        console.log(`app listening on port ${config.port}!`);
    });
})();
